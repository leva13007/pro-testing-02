const fn = (cb = () => {}) => {
  const mockFn = (...arg) => {
    mockFn.mock.calls.push(arg);
    return cb(...arg);
  }
  mockFn.mock = {calls: []};
  // mockFn.mockImplementation = newCb => (cb = newCb)
  return mockFn;
}

module.exports = {
  getWinner: fn((p1,p2) => p1),
};
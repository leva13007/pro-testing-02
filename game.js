const utils = require('./utils');

const run = (player1, player2) => {
  const numberToWin = 2;
  let player1Wins = 0;
  let player2Wins = 0;
  while (player1Wins < numberToWin && player2Wins < numberToWin) {
    const winner = utils.getWinner(player1, player2); // 'Player #1'
    if (player1 === winner) {
      player1Wins++;
    } else {
      player2Wins++;
    }
  }
  return player1Wins > player2Wins ? player1 : player2;
};

module.exports = run;

// console.log(run('Player #1', 'Player #2'));
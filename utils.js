const getWinner = (player1, player2) => {
  const gameKoef = Math.random();
  return gameKoef < 0.5 ? player1 : player2;
}

module.exports = {
  getWinner,
}
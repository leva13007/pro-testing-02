const utils = require("../utils");
const game = require("../game");
const assert = require("assert");

const fn = (cb) => {
  const mockFn = (...arg) => {
    mockFn.mock.calls.push(arg);
    return cb(...arg);
  }
  mockFn.mock = {calls: []};
  return mockFn;
}

const originalGetWinner = utils.getWinner;
utils.getWinner = fn((p1,p2) => p1);

const winner = game('Player #1', 'Player #2');
assert.strictEqual(winner, 'Player #1');
// console.log(utils.getWinner.mock);
// expect(winner).toBe('Player #1');
// expect(utils.getWinner).toHaveBeenCalledTimes(2);
// expect(utils.getWinner).toHaveBeenCalledWith('Player #1', 'Player #2');

// expect(utils.getWinner.mock.calls).toEqual( [ [ 'Player #1', 'Player #2' ], [ 'Player #1', 'Player #2' ] ]);
assert.deepStrictEqual(utils.getWinner.mock.calls,[ [ 'Player #1', 'Player #2' ], [ 'Player #1', 'Player #2' ] ])
utils.getWinner = originalGetWinner;

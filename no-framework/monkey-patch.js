const assert = require('assert');
const game = require('../game');
const utils = require('../utils');

const originalGetWinner = utils.getWinner;
utils.getWinner = (p1,p2) => p1;

const winner = game('Player #1', 'Player #2');
assert.strictEqual(winner, 'Player #1');

utils.getWinner = originalGetWinner;

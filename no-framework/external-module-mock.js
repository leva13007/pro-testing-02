const fn = (cb = () => {}) => {
  const mockFn = (...arg) => {
    mockFn.mock.calls.push(arg);
    return cb(...arg);
  }
  mockFn.mock = {calls: []};
  // mockFn.mockImplementation = newCb => (cb = newCb)
  return mockFn;
}

// const spyOn = (obj, prop) => {
//   const originValue = obj[prop];
//   obj[prop] = fn();
//   obj[prop].mockRestore = () => (obj[prop] = originValue);
// }

// console.log(require.cache);
require('../no-framework-mocks/utils');
const utilsPath = require.resolve('../utils');
const utilsMockPath = require.resolve('../no-framework-mocks/utils');
require.cache[utilsPath] = require.cache[utilsMockPath];


const utils = require("../utils");
const game = require("../game");
const assert = require("assert");

// console.log(require.cache)

// spyOn(utils, 'getWinner');
// utils.getWinner.mockImplementation((p1,p2) => p1)
// const originalGetWinner = utils.getWinner;
// utils.getWinner = fn((p1,p2) => p1);

const winner = game('Player #1', 'Player #2');
assert.strictEqual(winner, 'Player #1');
// console.log(utils.getWinner.mock);
// expect(winner).toBe('Player #1');
// expect(utils.getWinner).toHaveBeenCalledTimes(2);
// expect(utils.getWinner).toHaveBeenCalledWith('Player #1', 'Player #2');

// expect(utils.getWinner.mock.calls).toEqual( [ [ 'Player #1', 'Player #2' ], [ 'Player #1', 'Player #2' ] ]);
assert.deepStrictEqual(utils.getWinner.mock.calls,[ [ 'Player #1', 'Player #2' ], [ 'Player #1', 'Player #2' ] ])

delete require.cache[utilsPath];
// utils.getWinner.mockRestore();
// utils.getWinner = originalGetWinner;

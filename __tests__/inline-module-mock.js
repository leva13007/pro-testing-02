const utils = require("../utils");
const game = require("../game");

jest.mock('../utils', () => {
  return {
    getWinner: jest.fn((p1,p2) => p1),
  }
});

test('returns winner: spy', () => {
  // jest.spyOn(utils, 'getWinner');
  // utils.getWinner.mockImplementation((p1,p2) => p1)
  // const originalGetWinner = utils.getWinner;
  // utils.getWinner = jest.fn((p1,p2) => p1);

  const winner = game('Player #1', 'Player #2');
  // console.log(utils.getWinner.mock);
  expect(winner).toBe('Player #1');
  // expect(utils.getWinner).toHaveBeenCalledTimes(2);
  // expect(utils.getWinner).toHaveBeenCalledWith('Player #1', 'Player #2');

  expect(utils.getWinner.mock.calls).toEqual( [ [ 'Player #1', 'Player #2' ], [ 'Player #1', 'Player #2' ] ]);

  utils.getWinner.mockRestore();
  // utils.getWinner = originalGetWinner;
});
